// Package paulbecotte contains paulbecotte API versions.
//
// This file ensures Go source parsers acknowledge the paulbecotte package
// and any child packages. It can be removed if any other Go source files are
// added to this package.
package paulbecotte
