// Package v1alpha1 contains API Schema definitions for the paulbecotte v1alpha1 API group
// +k8s:deepcopy-gen=package,register
// +groupName=paulbecotte.com
package v1alpha1
