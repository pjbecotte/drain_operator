package controller

import (
	"gitlab.com/pjbecotte/drain-operator/pkg/controller/clusterdrainer"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, clusterdrainer.Add)
}
